// import { Component } from '@angular/core';
// import { IonicPage, NavController, NavParams } from 'ionic-angular';

// /**
//  * Generated class for the SignupPage page.
//  *
//  * See https://ionicframework.com/docs/components/#navigation for more info on
//  * Ionic pages and navigation.
//  */

// @IonicPage()
// @Component({
//   selector: 'page-signup',
//   templateUrl: 'signup.html',
// })
// export class SignupPage {

//   constructor(public navCtrl: NavController, public navParams: NavParams) {
//   }

//   ionViewDidLoad() {
//     console.log('ionViewDidLoad SignupPage');
//   }

// }


import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';

import { TabsPage } from '../tabs/tabs';


/**
 * Generated class for the SignupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  info={
    username:"",
    email:"",
    password:"",
    // repassword:"",
    playerID:"",
  };
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  signup(){
    var theLink = 'http://13.228.23.48/fyp1admin/public/api/attendee/register';
    let headers = new Headers(
      {
        'Content-Type' : 'application/x-www-form-urlencoded'
      });
    let options = new RequestOptions({ headers: headers });
    let params = new URLSearchParams()
    params.append("name", this.info.username);
    params.append("email", this.info.email);
    params.append("password", this.info.password);
    params.append("player_id", localStorage.getItem('userId'));
    this.http.post(theLink, params, options)
    .map(res => res.json())
    .subscribe(data => {  
      localStorage.setItem('name', data.name);
      localStorage.setItem('id', data.id)
      console.log(data);
      this.navCtrl.push(TabsPage);
    },
    err => {
      console.log(err);
    });
  }
}
