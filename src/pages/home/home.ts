import { Component } from '@angular/core';
import { NavController, AlertController, Events } from 'ionic-angular';
// import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { EventPage } from '../event/event';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
 
  posts: any;
  // payment: PayPalPayment = new PayPalPayment('10.10', 'USD', 'TV', 'sale');
	// currencies = ['EUR', 'USD', 'MYR'];
  // payPalEnvironment: string = 'AQNU0-1EJp_mYlBf6eoTomciTfxbDYk9KXNC_EMvtbURAL7bUrQBAq6hm1PsLsBH31tv6rgfA2GMnol2';
 
  constructor(public navCtrl: NavController,
              public alertCtrl: AlertController, 
              public http: Http,
              private payPal: PayPal,
              private event: Events) {
    this.doRefresh(0);
    // this.http.get('http://13.228.23.48/fyp1admin/public/api/getAllEvents').map(res => res.json()).subscribe(data => {
    //     this.posts = data;
    //     console.log(data);
    // });
    
 
  }

  showDetails(eventid){
    this.navCtrl.push(EventPage,{
      eventid: eventid,
    });
  }

  doRefresh(refresher){
    this.http.get('http://13.228.23.48/fyp1admin/public/api/getAllEvents').map(res => res.json()).subscribe(data => {
        this.posts = data;
 
      if(refresher != 0)
         refresher.complete();
    }); 
  }

  makePayment(amount, eventID, quantity){
    var totalAmount = amount*quantity;
    this.payPal.init({
      PayPalEnvironmentProduction: 'YOUR_PRODUCTION_CLIENT_ID',
      PayPalEnvironmentSandbox: 'AQNU0-1EJp_mYlBf6eoTomciTfxbDYk9KXNC_EMvtbURAL7bUrQBAq6hm1PsLsBH31tv6rgfA2GMnol2'
    }).then(() => {
      // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
      this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
        // Only needed if you get an "Internal Service Error" after PayPal login!
        //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
      })).then(() => {
        let payment = new PayPalPayment(totalAmount.toString(), 'MYR', 'Description', 'sale');
        this.payPal.renderSinglePaymentUI(payment).then(() => {
          this.saveAttandace(eventID, quantity, totalAmount);
          // Successfully paid
          
          // Example sandbox response
          //
          // {
          //   "client": {
          //     "environment": "sandbox",
          //     "product_name": "PayPal iOS SDK",
          //     "paypal_sdk_version": "2.16.0",
          //     "platform": "iOS"
          //   },
          //   "response_type": "payment",
          //   "response": {
          //     "id": "PAY-1AB23456CD789012EF34GHIJ",
          //     "state": "approved",
          //     "create_time": "2016-10-03T13:33:33Z",
          //     "intent": "sale"
          //   }
          // }
        }, () => {
          // Error or render dialog closed without being successful
        });
      }, () => {
        // Error in configuration
      });
    }, () => {
      // Error in initialization, maybe PayPal isn't supported or something else
    });
  }

  showPrompt(amount, eventID) {
    let prompt = this.alertCtrl.create({
      title: 'Quantity of tickets',
      message: "Enter a quantity of tickets you would like to purchase",
      inputs: [
        {
          name: 'quantity',
          placeholder: 'Quantity'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Proceed to Payment',
          handler: data => {
            // alert(data.quantity);
            this.makePayment(amount, eventID, data.quantity);
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();
  }

  saveAttandace(eventID, ticketQuantity, totalPrice){
    var theLink = 'http://13.228.23.48/fyp1admin/public/api/attendance/register';
    let headers = new Headers(
      {
        'Content-Type' : 'application/x-www-form-urlencoded'
      });
    let options = new RequestOptions({ headers: headers });
    let params = new URLSearchParams()
    params.append("attendeeID", localStorage.getItem('id'));
    params.append("eventID", eventID);
    params.append("ticketQuantity", ticketQuantity);
    params.append("totalPrice", totalPrice);
    this.http.post(theLink, params, options)
    .map(res => res.json())
    .subscribe(data => {  
      console.log(data);
      alert("Success");
    },
    err => {
      console.log(err);
    });
  }
}