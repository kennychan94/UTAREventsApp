import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  attendances;
  events;

  constructor(public navCtrl: NavController,
              public http: Http) {

  }

  ionViewDidEnter(){
    this.getAllAttendance();
  }

  getAllAttendance(){
    this.http.get('http://13.228.23.48/fyp1admin/public/api/attendance/getAllAttendance/'+localStorage.getItem('id')).map(res => res.json()).subscribe(data => {
      this.attendances = data;
    }); 

    this.http.get('http://13.228.23.48/fyp1admin/public/api/getAllEvents').map(res => res.json()).subscribe(data => {
        this.events = data;
    });
  }
}
