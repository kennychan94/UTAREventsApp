import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
 
@Component({
  selector: 'page-paypal',
  templateUrl: 'paypal.html'
})
export class PaypalPage {

    payment: PayPalPayment = new PayPalPayment('10.10', 'USD', 'TV', 'sale');
	currencies = ['EUR', 'USD'];
    payPalEnvironment: string = 'payPalEnvironmentSandbox';
    
  constructor(public navCtrl: NavController, public http: Http, private payPal: PayPal) {

  }

//   makePayment(){
//     this.payPal.init({
//         PayPalEnvironmentProduction: 'YOUR_PRODUCTION_CLIENT_ID',
//         PayPalEnvironmentSandbox: 'YOUR_SANDBOX_CLIENT_ID'
//       })
//     .then(onSuccess => {
//         console.log("init success")
//     })
//     .catch(onError => {
//         console.log("init failed", Error)
//     });
//   }

  makePayment() {
    this.payPal.init({
        PayPalEnvironmentProduction: "",
        PayPalEnvironmentSandbox: 'AQNU0-1EJp_mYlBf6eoTomciTfxbDYk9KXNC_EMvtbURAL7bUrQBAq6hm1PsLsBH31tv6rgfA2GMnol2'
    }).then(() => {
        this.payPal.prepareToRender(this.payPalEnvironment, new PayPalConfiguration({})).then(() => {
            this.payPal.renderSinglePaymentUI(this.payment).then((response) => {
                alert(`Successfully paid. Status = ${response.response.state}`);
                console.log(response);
            }, () => {
                console.error('Error or render dialog closed without being successful');
            });
        }, () => {
            console.error('Error in configuration');
        });
    }, () => {
        console.error('Error in initialization, maybe PayPal isn\'t supported or something else');
    });
}

  
}