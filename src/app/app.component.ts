import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';

import { OneSignal } from '@ionic-native/onesignal';

import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';

import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage:any = SignupPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private oneSignal: OneSignal, private payPal: PayPal) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      if(localStorage.getItem('name')!= null){
        this.rootPage = TabsPage;
      }else{
       this.rootPage = SignupPage;
      }
      this.oneSignalFunction();
      statusBar.styleDefault();
      splashScreen.hide();

    });
  }

  oneSignalFunction(){
    this.oneSignal.startInit('e7c4d63f-9a63-406a-aec1-b431dbd15c9d', '900827535030');
    this.oneSignal.setSubscription(true);
    this.oneSignal.getIds()
    .then((result)=>{
      localStorage.setItem('pushToken',result.pushToken);
      localStorage.setItem('userId',result.userId);
      console.log(result.pushToken);
      console.log(result.userId);
      console.log(result);
    })
    .catch(e=>console.log(e));
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

    this.oneSignal.handleNotificationReceived().subscribe(() => {
    // do something when notification is received
    });

    this.oneSignal.handleNotificationOpened().subscribe(() => {
      // do something when a notification is opened
    });

    this.oneSignal.endInit();
  }

}
